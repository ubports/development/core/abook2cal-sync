#ifndef BIRTHDAYCALENDAR_H
#define BIRTHDAYCALENDAR_H

#include <QObject>
#include <QContact>
#include <QDate>

#include <QOrganizerManager>

using namespace QtOrganizer;
using namespace QtContacts;

class CalendarBirthday
{

public:
    CalendarBirthday(const QDate &date, const QString &summary)
        : mDate(date), mSummary(summary) {}
    CalendarBirthday() {}


public:
    const QDate & date() const { return mDate; }
    const QString & summary() const { return mSummary; }

private:
    QDate mDate;
    QString mSummary;
};


class Calendar : public QObject
{
    Q_OBJECT
public:
    Calendar(QObject *parent = 0);

    //! Updates and saves Birthday of \a contact to calendar.
    void updateBirthdays(const QList<QContact> &contacts);

    //! Deletes \a contact birthday from calendar.
    void deleteBirthdays(const QList<QContactId> &contactIds);

    QHash<QContactId, CalendarBirthday> birthdays(const QList<QContactId> &contactIds);
    QHash<QContactId, CalendarBirthday> birthdays();

private:

    QString displayLabel(const QContact &contact) const;
    QOrganizerManager mManager;
    QOrganizerCollectionId mBirthdaysCalendarId;
};

#endif // BIRTHDAYCALENDAR_H
