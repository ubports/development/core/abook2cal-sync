TEMPLATE = app
TARGET = lomiri-abook2cal-syncd

QT += core

CONFIG += \
    c++11 \
    link_pkgconfig

PKGCONFIG += \
    Qt5Contacts \
    Qt5Organizer \
    qtcontacts-sqlite-qt5-extensions

SOURCES += \
    calendar.cpp \
    main.cpp \
    qtcontacts2edscalendar.cpp

HEADERS += \
    calendar.h \
    qtcontacts2edscalendar.h


target.path = /usr/bin

service.files = systemd/lomiri-abook2cal-syncd.service
service.path = $$system(pkg-config --variable=systemduserunitdir systemd)

INSTALLS += target \
            service
