#include "calendar.h"

#include <QOrganizerManager>
#include <QContactCollection>
#include <QOrganizerItemCollectionFilter>
#include <QOrganizerItemIntersectionFilter>
#include <QOrganizerEvent>
#include <QContactName>
#include <QContactBirthday>
#include <QContactDisplayLabel>
#include <QContactNickname>
#include <QContactOrganization>
#include <QContactEmailAddress>
#include <QDebug>
#include <QContactDetailFilter>
#include <QOrganizerItemDetailFieldFilter>
#include <QOrganizerItemIdFilter>
#include <QOrganizerItemDetailFilter>
#include <QOrganizerItemGuid>
#include <QByteArray>
#include <QOrganizerItemExtendedDetail>
#include <QOrganizerItemUnionFilter>

#define COLLECTION_BIRTHDAY_NAME "QtContactBirthdays"

using namespace QtOrganizer;

Calendar::Calendar(QObject *parent):
    QObject(parent),
    mManager(QStringLiteral("eds"))
{

    QOrganizerCollection currentBirthayCal;
    QList<QOrganizerCollection> collections = mManager.collections();

    // "Birthdays & Anniversaries" -> selected = false
    for (const QOrganizerCollection &col: collections) {
            QString collectionName = col.metaData(QOrganizerCollection::KeyName).toString();
        if (collectionName == COLLECTION_BIRTHDAY_NAME) {
            mBirthdaysCalendarId = QOrganizerCollectionId(col.id());
            qDebug() << " Found BirthDay Calendar:" << mBirthdaysCalendarId;
        }

        if (collectionName == "Birthdays & Anniversaries" && col.extendedMetaData("collection-selected").toBool()) {
            currentBirthayCal = QOrganizerCollection(col);
            qDebug() << " Found initial BirthDay Calendar:" << currentBirthayCal.id();
        }
    }

    if (mBirthdaysCalendarId.isNull()) {
        // we create a Calendar

        QOrganizerCollection newCol;
        newCol.setMetaData(QOrganizerCollection::KeyName, COLLECTION_BIRTHDAY_NAME);
        newCol.setMetaData(QOrganizerCollection::KeyColor, "#77216f");
        newCol.setExtendedMetaData("collection-type", "Calendar");
        newCol.setExtendedMetaData("collection-selected", true);

        bool saved = mManager.saveCollection(&newCol);
        if (!saved) {
            qWarning() << " failed to create Birthday Calendar, contact birthdays will not be reported to Calendar";
        } else {
            qDebug() << " Successfully created Birthday Calendar " << newCol.id();
            mBirthdaysCalendarId = newCol.id();

            if (!currentBirthayCal.id().isNull()) {
                 currentBirthayCal.setExtendedMetaData("collection-selected", false);
                bool saved = mManager.saveCollection(&currentBirthayCal);
                if (!saved) {
                    qWarning() << " failed to deselect initial Birthday Calendar";
                } else {
                    qDebug() << " Successfully deselect initial Birthday Calendar ";
                }
            }
        }
    }
}

QString Calendar::displayLabel(const QContact &contact) const
{
    QString displayLabel = contact.detail<QContactDisplayLabel>().label();
    if (displayLabel.isEmpty()) {
        displayLabel = contact.detail<QContactNickname>().nickname();
    }
    if (displayLabel.isEmpty()) {
        displayLabel = contact.detail<QContactOrganization>().name();
    }
    if (displayLabel.isEmpty()) {
        displayLabel = contact.detail<QContactEmailAddress>().emailAddress();
    }

    return displayLabel;
}

void Calendar::updateBirthdays(const QList<QContact> &contacts)
{

    for (const QContact &contact: contacts) {

        QString eventId = contact.id().toString();

        const QDateTime contactBirthday = contact.detail<QContactBirthday>().dateTime();
        if (contactBirthday.isNull()) {
            qWarning() << Q_FUNC_INFO << "Contact without birthday, local ID: " << contact.id();
            continue;
        }

        QString label = displayLabel(contact);
        if (label.isEmpty()) {
            qWarning() << Q_FUNC_INFO << "Contact without name to use " << contact.id();
            continue;
        }

        QOrganizerItemCollectionFilter collFilter;
        collFilter.setCollectionId(mBirthdaysCalendarId);

        QOrganizerItemExtendedDetail xuidDetail;
        xuidDetail.setName(QStringLiteral("X-CONTACT-UID"));
        xuidDetail.setData(eventId);
        QOrganizerItemDetailFilter idFilter;
        idFilter.setDetail(xuidDetail);

        QOrganizerItemIntersectionFilter isf;
        isf.append(collFilter);
        isf.append(idFilter);

        QList<QOrganizerItem> items = mManager.items(QDateTime(), QDateTime(), isf);

        QOrganizerEvent event;
        if (!items.isEmpty()) {
            qDebug() << "ok, got an update: " << contact.id();
            event = static_cast<QOrganizerEvent>(items.at(0));
        }

        event.setCollectionId(mBirthdaysCalendarId);
        event.setDisplayLabel(label);
        event.setStartDateTime(contactBirthday);
        event.setAllDay(true);
        event.setData("X-CONTACT-UID", eventId);

        QOrganizerRecurrenceRule rrule;
        rrule.setFrequency(QOrganizerRecurrenceRule::Yearly);
        event.setRecurrenceRule(rrule);

        bool saved = mManager.saveItem(&event);
        qDebug() << "mManager.saveItem() " << saved;
    }
}

void Calendar::deleteBirthdays(const QList<QContactId> &contactIds)
{
    if (contactIds.isEmpty()) {
        qDebug() << "deleteBirthdays nothing to remove";
        return;
    }

    QOrganizerItemCollectionFilter collFilter;
    collFilter.setCollectionId(mBirthdaysCalendarId);

    QOrganizerItemUnionFilter idsFilter;

    for (const QContactId &id: contactIds) {
        QOrganizerItemExtendedDetail xuidDetail;
        xuidDetail.setName(QStringLiteral("X-CONTACT-UID"));
        xuidDetail.setData(id.toString());

        QOrganizerItemDetailFilter idFilter;
        idFilter.setDetail(xuidDetail);

        idsFilter.append(idFilter);
    }

    QOrganizerItemIntersectionFilter isf;
    isf.append(collFilter);
    isf.append(idsFilter);

    const QList<QOrganizerItem> items = mManager.items(QDateTime(), QDateTime(), isf);
    QList<QOrganizerItemId> itemIds;
    for (const QOrganizerItem &item: items) {
        itemIds << item.id();
    }

    if (!itemIds.isEmpty()) {
        bool removed = mManager.removeItems(qAsConst(itemIds));
        if (!removed) {
            qWarning() << "can't remove birthdays" << mManager.error();
        } else {
            qDebug() << "Removed " << itemIds.size() << " events";
        }
    }
}

QHash<QContactId, CalendarBirthday> Calendar::birthdays(const QList<QContactId> &contactIds)
{

    QOrganizerItemCollectionFilter collFilter;
    collFilter.setCollectionId(mBirthdaysCalendarId);

    QOrganizerItemUnionFilter idsFilter;

    for (const QContactId &ctId: contactIds) {
        QString eventId = ctId.toString();

        QOrganizerItemExtendedDetail xuidDetail;
        xuidDetail.setName(QStringLiteral("X-CONTACT-UID"));
        xuidDetail.setData(eventId);

        QOrganizerItemDetailFilter idFilter;
        idFilter.setDetail(xuidDetail);

        idsFilter.append(idFilter);
    }

    QOrganizerItemIntersectionFilter isf;
    isf.append(collFilter);
    isf.append(idsFilter);

    QHash<QContactId, CalendarBirthday> result;
    const QList<QOrganizerItem> items = mManager.items(QDateTime(), QDateTime(), isf);
    for (const QOrganizerItem &birthdayItem: items) {
        QOrganizerEvent ev = static_cast<QOrganizerEvent>(birthdayItem);

        const QString eventUid = ev.data(QStringLiteral("X-CONTACT-UID")).toString();
        const QContactId contactId = QContactId::fromString(eventUid);

        result.insert(contactId, CalendarBirthday(ev.startDateTime().date(), birthdayItem.displayLabel()));
    }

    return result;
}

QHash<QContactId, CalendarBirthday> Calendar::birthdays()
{
    QOrganizerItemCollectionFilter collFilter;
    collFilter.setCollectionId(mBirthdaysCalendarId);

    QHash<QContactId, CalendarBirthday> result;

    const QList<QOrganizerItem> birthDays = mManager.items(QDateTime(), QDateTime(), collFilter);
    for (const QOrganizerItem &birthdayItem: birthDays) {

        QOrganizerEvent ev = static_cast<QOrganizerEvent>(birthdayItem);

        const QString eventUid = ev.data(QStringLiteral("X-CONTACT-UID")).toString();
        const QContactId contactId = QContactId::fromString(eventUid);

        result.insert(contactId, CalendarBirthday(ev.startDateTime().date(), birthdayItem.displayLabel()));
    }

    return result;
}
