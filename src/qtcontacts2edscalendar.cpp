/*
 * Copyright (C) 2024 UBports Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QCoreApplication>
#include <QContactDetailFilter>
#include <QDebug>
#include <QDir>
#include <QOrganizerManager>
#include <QContactBirthday>
#include <QContactDetailFilter>
#include <QContactFetchRequest>
#include <QContactIdFilter>
#include <QContactDisplayLabel>
#include <QContactCollectionFilter>
#include <QTimer>

#include <qtcontacts-extensions.h>
#include <qtcontacts-extensions_impl.h>

#include "calendar.h"
#include "qtcontacts2edscalendar.h"

using namespace QtOrganizer;
using namespace QtContacts;

const int UPDATE_TIMEOUT = 1000; // ms


QMap<QString, QString> managerParameters()
{
    // We don't need to handle presence changes, so report them separately and ignore them
    QMap<QString, QString> parameters;
    parameters.insert(QString::fromLatin1("mergePresenceChanges"), QString::fromLatin1("false"));
    return parameters;
}


QtContacts2EdsCalendar::QtContacts2EdsCalendar(QObject *parent):
    QObject(parent),
    mManager(QStringLiteral("org.nemomobile.contacts.sqlite"), managerParameters()),
    mRequest(new QContactFetchRequest),
    mUpdateAllPending(false),
    mSyncMode(Incremental)
{
}

void QtContacts2EdsCalendar::start()
{
    mCalendar = new Calendar(this);

    QObject::connect(&mManager, &QContactManager::contactsAdded,
                     this, &QtContacts2EdsCalendar::contactsChanged);
    QObject::connect(&mManager, &QContactManager::contactsChanged,
                     this, &QtContacts2EdsCalendar::contactsChanged);
    QObject::connect(&mManager, &QContactManager::contactsRemoved,
                     this, &QtContacts2EdsCalendar::contactsRemoved);

    QObject::connect(&mManager, &QContactManager::dataChanged, this, &QtContacts2EdsCalendar::updateAllBirthdays);

    updateAllBirthdays();

    mUpdateTimer.setInterval(UPDATE_TIMEOUT);
    mUpdateTimer.setSingleShot(true);
    QObject::connect(&mUpdateTimer, &QTimer::timeout, this, &QtContacts2EdsCalendar::onUpdateQueueTimeout);

}

void QtContacts2EdsCalendar::contactsChanged(const QList<QContactId>& contacts)
{
    qWarning() << Q_FUNC_INFO << contacts.size();
    foreach (const QContactId &id, contacts)
        mUpdatedContacts.insert(id);

    // Just restart the timer - if it doesn't expire, we can afford to wait
    mUpdateTimer.start();
}

void QtContacts2EdsCalendar::contactsRemoved(const QList<QContactId>& contacts)
{
    qWarning() << Q_FUNC_INFO << contacts.size();
    mCalendar->deleteBirthdays(contacts);

}

void QtContacts2EdsCalendar::updateAllBirthdays() {
     qWarning() << Q_FUNC_INFO;
    if (mRequest->isActive()) {
        mUpdateAllPending = true;
    } else {
        // Fetch every contact with a birthday.
        QContactDetailFilter filter;
        filter.setDetailType(QContactBirthday::Type, QContactBirthday::FieldBirthday);
        fetchContacts(filter , FullSync);
    }
}

void QtContacts2EdsCalendar::fetchContacts(const QContactFilter &filter, SyncMode mode)
{
    // Set up the fetch request object
    mRequest->setManager(&mManager);

    QContactFetchHint fetchHint;
    fetchHint.setDetailTypesHint(QList<QContactDetail::DetailType>() << QContactBirthday::Type << QContactDisplayLabel::Type);
    fetchHint.setOptimizationHints(QContactFetchHint::NoRelationships |
                                   QContactFetchHint::NoActionPreferences |
                                   QContactFetchHint::NoBinaryBlobs);
    mRequest->setFetchHint(fetchHint);

    // Only fetch aggregate contacts
    QContactCollectionFilter aggregateFilter;
    aggregateFilter.setCollectionId(QtContactsSqliteExtensions::aggregateCollectionId(mManager.managerUri()));
    mRequest->setFilter(filter & aggregateFilter);

    QObject::connect(mRequest.data(), &QContactFetchRequest::stateChanged, this, &QtContacts2EdsCalendar::onRequestStateChanged);

    if (!mRequest->start()) {
        qWarning() << Q_FUNC_INFO << "Unable to start birthday contact fetch request";
    } else {
        qDebug() << "Birthday contacts fetch request started";
        mSyncMode = mode;
    }
}

void QtContacts2EdsCalendar::onRequestStateChanged(QContactAbstractRequest::State newState)
{
    if (newState == QContactAbstractRequest::FinishedState) {

        if (mRequest->error() != QContactManager::NoError) {
            qWarning() << Q_FUNC_INFO << "Error during birthday contact fetch request, code:" << mRequest->error();
        } else {
            if (mSyncMode == FullSync) {
                syncBirthdays(mRequest->contacts());

                // Create the stamp file only after a successful full sync.
                // TODO createStampFile();
            } else {
                updateBirthdays(mRequest->contacts());
            }
        }

        // We're finished with this request, clear it out to drop any contact data
        // (although don't delete it directly, as we're currently handling a signal from it)
        mRequest.take()->deleteLater();
        mRequest.reset(new QContactFetchRequest);
    } else if (newState == QContactAbstractRequest::CanceledState) {
        qDebug() << "Birthday contacts fetch request canceled";
    } else {
        // Request still in progress
        return;
    }

    if (mUpdateAllPending) {
        // We need to update all birthdays
        mUpdateAllPending = false;
        updateAllBirthdays();
    } else if (!mUpdatedContacts.isEmpty() && !mUpdateTimer.isActive()) {
        // If some updated contacts weren't requested, we need to go again
        mUpdateTimer.start();
    }
}

void QtContacts2EdsCalendar::updateBirthdays(const QList<QContact> &changedBirthdays)
{
    qDebug() << "updateBirthdays count: " << changedBirthdays.size();

    QList<QContact> ctsToUpdate;
    QList<QContactId> ctsToDelete;

    QList<QContactId> changedBirthdaysIds;
    for(const QContact &ct: changedBirthdays){
        changedBirthdaysIds << ct.id();
    }
    QHash<QContactId, CalendarBirthday> oldBirthdays = mCalendar->birthdays(changedBirthdaysIds);

    foreach (const QContact &contact, changedBirthdays) {
        const QContactBirthday contactBirthday = contact.detail<QContactBirthday>();
        const QString contactDisplayLabel = contact.detail<QContactDisplayLabel>().label();


        QHash<QContactId, CalendarBirthday>::Iterator it = oldBirthdays.find(contact.id());

        if (oldBirthdays.end() != it) {
            const CalendarBirthday &calendarBirthday = *it;
            // Display label or birthdate was removed from the contact, so delete it from the calendar.
            if (contactDisplayLabel.isEmpty() || contactBirthday.date().isNull()) {
                if (!calendarBirthday.date().isNull()) {
                    qDebug() << "Contact: " << contact.id() << " removed birthday or displayLabel, so delete the calendar event";
                    ctsToDelete << contact.id();
                }
                // Display label or birthdate was changed on the contact, so update the calendar.
            } else if ((contactDisplayLabel != calendarBirthday.summary()) ||
                       (contactBirthday.date() != calendarBirthday.date())) {
                qDebug() << "Contact with calendar birthday: " << calendarBirthday.date()
                         << " and calendar displayLabel: " << calendarBirthday.summary()
                         << " changed details to: " << contactBirthday.date() << contactDisplayLabel
                         << ", so update the calendar event";

                //mCalendar->updateBirthday(contact);
                ctsToUpdate << contact;
            }
        } else {
            ctsToUpdate << contact;
        }
    }

    if (!ctsToUpdate.isEmpty()) {
        mCalendar->updateBirthdays(ctsToUpdate);
    }

    if (!ctsToDelete.isEmpty()) {
        mCalendar->deleteBirthdays(ctsToDelete);
    }

}

void QtContacts2EdsCalendar::syncBirthdays(const QList<QContact> &birthdayContacts)
{
    qWarning() << Q_FUNC_INFO << "birthdayContacts" << birthdayContacts.size();
    QHash<QContactId, CalendarBirthday> oldBirthdays = mCalendar->birthdays();

    QList<QContact> ctsToUpdate;

    // Check all birthdays from the contacts if the stored calendar item is up-to-date
    foreach (const QContact &contact, birthdayContacts) {
        const QString contactDisplayLabel = contact.detail<QContactDisplayLabel>().label();
        if (contactDisplayLabel.isEmpty()) {
            qDebug() << "Contact: " << contact << " has no displayLabel, so not syncing to calendar";
            continue;
        }
        QHash<QContactId, CalendarBirthday>::Iterator it = oldBirthdays.find(contact.id());

        if (oldBirthdays.end() != it) {
            const QContactBirthday contactBirthday = contact.detail<QContactBirthday>();
            const CalendarBirthday &calendarBirthday = *it;

            // Display label or birthdate was changed on the contact, so update the calendar.
            if ((contactDisplayLabel != calendarBirthday.summary()) ||
                (contactBirthday.date() != calendarBirthday.date())) {
                qDebug() << "Contact with calendar birthday: " << contactBirthday.date()
                         << " and calendar displayLabel: " << calendarBirthday.summary()
                         << " changed details to: " << contact << ", so update the calendar event";

                ctsToUpdate << contact;
            }

            // Birthday exists, so not a garbage one
            oldBirthdays.erase(it);
        } else {
            // Create new birthday
            ctsToUpdate << contact;
        }
    }

    if (!ctsToUpdate.isEmpty()) {
        mCalendar->updateBirthdays(ctsToUpdate);
    }


    // Remaining old birthdays in the calendar db do not did not match any contact, so remove them.
    mCalendar->deleteBirthdays(oldBirthdays.keys());

}

void QtContacts2EdsCalendar::onUpdateQueueTimeout()
{
    if (mRequest->isActive()) {
        // The timer will be restarted by completion of the active request
        return;
    }

    QList<QContactId> contactIds(mUpdatedContacts.toList());

    // If we request too many contact IDs, we will exceed the SQLite bound variable limit
    const int batchSize = 200;
    if (contactIds.count() > batchSize) {
        mUpdatedContacts = contactIds.mid(batchSize).toSet();
        contactIds = contactIds.mid(0, batchSize);
    } else {
        mUpdatedContacts.clear();
    }

    QContactIdFilter fetchFilter;
    fetchFilter.setIds(contactIds);

    fetchContacts(fetchFilter, Incremental);
}
