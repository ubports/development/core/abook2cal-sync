/*
 * Copyright 2024 UBports Foundation
 *
 * This file is part of address-book-service.
 *
 * sync-monitor is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * contact-service-app is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <QtCore/QCoreApplication>
#include "qtcontacts2edscalendar.h"

#include <QDebug>
#include <QSocketNotifier>

#include <signal.h>
#include <sys/socket.h>
#include <unistd.h>

static int sigFd[2] = {0, 0};


static void unixSignalHandler(int)
{
    // Write a byte on the socket to activate the socket listener
    char a = 1;
    ::write(sigFd[0], &a, sizeof(a));
}

static void setupUnixSignalHandlers()
{
    struct sigaction sigterm, sigint;

    sigterm.sa_handler = unixSignalHandler;
    sigemptyset(&sigterm.sa_mask);
    sigterm.sa_flags = SA_RESTART;

    if (sigaction(SIGTERM, &sigterm, 0) < 0) {
        qWarning() << "Could not setup signal handler for SIGTERM";
        return;
    }

    sigint.sa_handler = unixSignalHandler;
    sigemptyset(&sigint.sa_mask);
    sigint.sa_flags = SA_RESTART;

    if (sigaction(SIGINT, &sigint, 0) < 0) {
        qWarning() << "Could not setup signal handler for SIGINT";
        return;
    }
}

int main(int argc, char **argv) {
    QCoreApplication app(argc, argv);

    QtContacts2EdsCalendar sqlite2Eds;

    if (::socketpair(AF_UNIX, SOCK_STREAM, 0, sigFd) != 0) {
        qWarning() << "Could not setup UNIX signal handler";
    }

    QSocketNotifier mSignalNotifier(sigFd[1], QSocketNotifier::Read);
    QObject::connect(&mSignalNotifier, &QSocketNotifier::activated, [&]() {
        mSignalNotifier.setEnabled(false);
        char dummy;
        if (::read(sigFd[1], &dummy, sizeof(dummy)) != sizeof(dummy)) {
            qWarning() << "Unable to complete read from sigFd" << errno;
        }
        qDebug() << "Received quit signal";

        QCoreApplication::quit();
        mSignalNotifier.setEnabled(true);
    });

    setupUnixSignalHandlers();

    QTimer::singleShot(0, [&sqlite2Eds]() {
        // will run right after event loop starts
        sqlite2Eds.start();
    });

    return app.exec();
}
