#ifndef QTCONTACTS2EDSCALENDAR_H
#define QTCONTACTS2EDSCALENDAR_H

/*
 * Copyright (C) 2024 UBports Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "calendar.h"

#include <QContactFetchRequest>
#include <QContactFilter>
#include <QContactId>
#include <QContactManager>
#include <QSet>
#include <QTimer>

using namespace QtOrganizer;
using namespace QtContacts;


class QtContacts2EdsCalendar : public QObject{

    Q_OBJECT

    enum SyncMode {
        Incremental,
        FullSync
    };

public:

    QtContacts2EdsCalendar(QObject *parent = 0);

    void contactsChanged(const QList<QContactId>& contacts);
    void contactsRemoved(const QList<QContactId>& contacts);
    void updateAllBirthdays();
    void fetchContacts(const QContactFilter &filter, SyncMode mode);
    void onRequestStateChanged(QContactAbstractRequest::State newState);
    void updateBirthdays(const QList<QContact> &changedBirthdays);
    void syncBirthdays(const QList<QContact> &birthdayContacts);
    void onUpdateQueueTimeout();

public Q_SLOTS:
    void start();

private:
    Calendar *mCalendar;
    QContactManager mManager;
    QScopedPointer<QContactFetchRequest> mRequest;
    QSet<QContactId> mUpdatedContacts;
    QTimer mUpdateTimer;
    bool mUpdateAllPending;
    SyncMode mSyncMode;


};

#endif // QTCONTACTS2EDSCALENDAR_H
